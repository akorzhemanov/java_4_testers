package ru.tsystems;

/**
 * Main class to calculate some measurements.
 */
public class Main {

    /**
     * Contains measurements coefficients to convert.
     */
    static class Coefficients {

        static class Metres {
            private static final double TO_MILES = 0.000621371;
            private static final double TO_YARDS = 1.09361296;
            private static final double TO_FEETS = 3.2808388799999997;
        }

        static class Kilograms {
            private static final double TO_LB = 2.20462;
            private static final double TO_OZ = 35.2739199982575;
        }

    }

    private final double lenghtInMeter;
    private final double weightInKilo;

    private double lengthInMiles;
    private double lengthInYards;
    private double lengthInFeets;

    private double weightInLb;
    private double weightInOz;

    public Main(double lengthInMeter, double weightInKilo) {
        this.lenghtInMeter = lengthInMeter;
        this.weightInKilo = weightInKilo;

        lengthInMiles = convertMetresToMiles(this.lenghtInMeter);
        lengthInYards = convertMetresToYards(this.lenghtInMeter);
        lengthInFeets = convertMetresToFeets(this.lenghtInMeter);

        weightInLb = convertKilogramsToLb(this.weightInKilo);
        weightInOz = convertKilogramsToOz(this.weightInKilo);
    }

    public static void main(String[] args) {
        Main m1 = new Main(1234.56, 789.99);
        printToConsole(m1);

        System.out.println("\n***************************************\n");

        Main m2 = new Main(50.11, 0.543);
        printToConsole(m2); 
    }

    /**
     * Out measurements to the console with 2 digits after dot.
     *
     * @param m
     */
    public static void printToConsole(Main m) {
        if (m == null) {
            throw new IllegalArgumentException("Not possible to obtain measurements");
        }
        System.out.format("%.2f metres is equivalent to %.2f miles%n", m.getLenghtInMeter(), m.getLengthInMiles());
        System.out.format("%.2f metres is equivalent to %.2f yards%n", m.getLenghtInMeter(), m.getLengthInYards());
        System.out.format("%.2f metres is equivalent to %.2f feets%n", m.getLenghtInMeter(), m.getLengthInFeets());
        System.out.println();
        System.out.format("%.2f kilograms is equivalent to %.2f lb %n", m.getWeightInKilo(), m.getWeightInLb());
        System.out.format("%.2f kilograms is equivalent to %.2f oz %n", m.getWeightInKilo(), m.getWeightInOz());
    }

    private static double convertMetresToMiles(double metres) {
        return metres * Coefficients.Metres.TO_MILES;
    }

    private static double convertMetresToYards(double metres) {
        return metres * Coefficients.Metres.TO_YARDS;
    }

    private static double convertMetresToFeets(double metres) {
        return metres * Coefficients.Metres.TO_FEETS;
    }

    private static double convertKilogramsToLb(double kilograms) {
        return kilograms * Coefficients.Kilograms.TO_LB;
    }

    private double convertKilogramsToOz(double kilograms) {
        return kilograms * Coefficients.Kilograms.TO_OZ;
    }

    public double getLenghtInMeter() {
        return lenghtInMeter;
    }

    public double getWeightInKilo() {
        return weightInKilo;
    }

    public double getLengthInMiles() {
        return lengthInMiles;
    }

    public double getLengthInYards() {
        return lengthInYards;
    }

    public double getLengthInFeets() {
        return lengthInFeets;
    }

    public double getWeightInLb() {
        return weightInLb;
    }

    public double getWeightInOz() {
        return weightInOz;
    }
}